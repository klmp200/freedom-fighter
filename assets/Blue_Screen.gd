extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	set_fixed_process(true)
	# Called every time the node is added to the scene.
	# Initialization here

func _fixed_process():
	
	set_global_pos(get_parent().get_node("Drone").get_global_pos())
