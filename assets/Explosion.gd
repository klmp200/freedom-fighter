extends Area2D

var frames = 0
var lifeTime = 6

func _ready():
	set_fixed_process(true)
	pass

func _fixed_process(delta):
	frames += 1
	if (frames * delta > 3):
		self.queue_free()

func _on_Explosion_body_enter( body ):
	if (body.is_in_group("Enemy")):

		print(body.get_name())

		if body.get_name().match("Civilian*"):
			call_deferred("kill_civilian_queue", body.get_parent())

		elif body.get_name().match("Enemy*"):
			call_deferred("kill_tank_queue", body.get_parent())




func kill_tank_queue(tank):

	get_node("/root/Map").kill_tank(tank)


func kill_civilian_queue(civilian):

	get_node("/root/Map").kill_civilian(civilian)
