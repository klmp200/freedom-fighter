extends KinematicBody2D

const MOVE_SPEED = 500
const GDELAIS_TEMPS = 1
const SDELAIS_TEMPS = 0.1
var bomb = load("res://assets/Bomb.tscn")
var mitraillette = load("res://assets/Mitraillette.tscn")
var Gframes = 9999
var Sframes = 9999
var SchauffeLimit = 8
var CanShoot = true
var Schauffe = 0
var MaxHP = 10
var HP = MaxHP
var end = false

func get_cooldown():
	if (Schauffe>=SchauffeLimit):
		return 100
	else:
		return Schauffe*100/SchauffeLimit
func _ready():
	set_fixed_process(true)
	
func _fixed_process(delta):

	var mousePosition = get_global_mouse_pos()
	Gframes += 1
	Sframes += 1
	var motion = Vector2(0,0)
	#Ne pas oublier set les touches


	if (Input.is_action_pressed("move_up")):
		motion += Vector2(0,-1)
	elif (Input.is_action_pressed("move_down")):
		motion += Vector2(0,1)
	
	if (Input.is_action_pressed("move_right")):
		motion += Vector2(1,0)
	elif (Input.is_action_pressed("move_left")):
		motion += Vector2(-1,0)

	if (Input.is_action_pressed("ui_Sshoot") and Gframes * delta > GDELAIS_TEMPS):
		var bombMouse = bomb.instance()
		bombMouse.set_pos(get_pos())
		get_parent().add_child(bombMouse)
		Gframes = 0
		get_node("songBomb").play(0)

	#Make drone follow pointer
	set_global_rot(get_global_pos().angle_to_point(mousePosition))
		
	if (Sframes * delta > SDELAIS_TEMPS):

		if (Input.is_action_pressed("ui_Gshoot") and CanShoot):

				
			var mitrailletteMouse = mitraillette.instance()
			mitrailletteMouse.set_pos(get_pos())
			get_parent().add_child(mitrailletteMouse)
			Sframes = 0
			Schauffe += 0.1
			get_node("songTir").play(0)

		elif (Schauffe > 0.1 * delta):

			Schauffe -= 1 * delta


	if (Schauffe >= SchauffeLimit):

		CanShoot = false
		get_node("Schauffe").play(0)

	if (get_cooldown() <= 1):

		CanShoot = true


	motion = motion.normalized() * delta * MOVE_SPEED
	if !end:
		move(motion)
	
	get_node("../UICanvas/UI").update_cooldown(get_cooldown())
	get_node("../UICanvas/UI").update_hp(HP*100/MaxHP)
