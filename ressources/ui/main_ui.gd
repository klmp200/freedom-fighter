extends Control

var animation_list = [
	preload("res://ressources/subliminal/sub1.tscn"),
	preload("res://ressources/subliminal/sub2.tscn"),
	preload("res://ressources/subliminal/sub3.tscn"),
	preload("res://ressources/subliminal/sub4.tscn"),
	preload("res://ressources/subliminal/sub5.tscn"),
	preload("res://ressources/subliminal/sub6.tscn"),
]
var is_in_game = false

func _ready():
	get_node("Button").connect("pressed", self,"_on_button_pressed")

func _input_event(ev):
	if (ev.type==InputEvent.MOUSE_BUTTON and ev.pressed and not is_in_game):
		get_node("SplashScreen").queue_free()
		is_in_game = true
		update()

func update_cooldown(percent):
	get_node("Cooldown").set_value(percent)

func update_hp(percent):
	get_node("HP").set_value(percent)

func launch_random_animation():
	var rnd = animation_list[randi() % animation_list.size()].instance()
	get_node("AnimationSpawner").add_child(rnd)

func launch_random_animation_x_times(times):
	var i = 0
	while (i<times):
		launch_random_animation()
		i+=1


func display_ending(kill, life): #hardcoded omg

	var map = get_node("/root/Map")
	if(!map.get_node("Drone").end):
		map.get_node("Drone").end = true
		#map.get_node("City").queue_free()
		#map.get_node("StreamPlayer").queue_free()
		#map.get_node("Mouse").destroy()
		#map.get_node("Drone").queue_free()
		if (kill > 20):
			var badEnd= load("res://assets/bad_end.tscn")
			var badEndInstance = badEnd.instance()
			badEndInstance.set_global_pos(map.get_node("Drone").get_global_pos())
			map.add_child(badEndInstance)

		elif (life == 0):
			var blueScreen = load("res://assets/Blue_Screen.tscn")
			var blueScreenInstance = blueScreen.instance()
			blueScreenInstance.set_global_pos(map.get_node("Drone").get_global_pos())
			map.add_child(blueScreenInstance)
		else:
			var goodEnd = load("res://assets/good end.tscn")
			var goodEndInstance = goodEnd.instance()
			goodEndInstance.set_global_pos(map.get_node("Drone").get_global_pos())
			map.add_child(goodEndInstance)


func _on_button_pressed():

	display_ending(get_node("/root/Map").get_kill(), -1)
