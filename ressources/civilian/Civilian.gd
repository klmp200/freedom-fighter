extends Node

var speed = 150
var position
var fearDistance = 800
var life = 5
var burka = preload("res://assets/burka.tscn")
var taliban = preload("res://assets/taliban.tscn")
var randomSprite = 0
	
func _ready():

	#ARE YOU BURKA ? IM NOT, BUT WELL IT'S OK TE BE BURKA
	randomize()
	randomSprite = randi()%2
	if randomSprite == 0:
		var burkaInstance = burka.instance()
		get_node("CivilianKBody").add_child(burkaInstance)
		burkaInstance.set_name("sprite")

	else: 
		var talibanInstance = taliban.instance()
		get_node("CivilianKBody").add_child(talibanInstance)
		talibanInstance.set_name("sprite")

	
	set_fixed_process(true)


func _fixed_process(delta):


	position = get_node("CivilianKBody").get_global_pos()
	var playerPosition = self.get_parent().get_node("Drone").get_pos()
	var movementVector = position - playerPosition
	var animation = get_node("CivilianKBody/sprite/marche")
	var angle = get_node("CivilianKBody").get_global_pos().angle_to(movementVector) + 1.1 

	if movementVector.length() < fearDistance:
		get_node("CivilianKBody").set_global_pos(position + movementVector.normalized() * speed * delta)
		get_node("CivilianKBody").set_global_rot(angle) #yy work
		
		#animations
		if !animation.is_playing():
			animation.play("marche", 1, 1, false)

	elif animation.is_playing():
		animation.stop()
	


