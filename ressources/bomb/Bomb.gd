extends KinematicBody2D

const DELAIS_EXPLOSION = 3
const MAX_LENGTH_LAUNCH = 500
const MAX_SPEED = 10
const RATIO_DIM_REDUCTION = 0.01
const INIT_TAILLE = 2
const SPEED_REDUCTION = 50
const RATIO_SPEED_REDUCTION = 0.98
var frames = 0
var motion = Vector2()
var defini = false

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):

	frames += 1
	set_scale(Vector2(INIT_TAILLE - frames * RATIO_DIM_REDUCTION, INIT_TAILLE - frames * RATIO_DIM_REDUCTION))

	if (defini == false):

		var deltaPosition = get_global_mouse_pos() - get_node("../Drone").get_pos()

		if (deltaPosition.length() > MAX_LENGTH_LAUNCH):

			motion = deltaPosition.normalized() * MAX_SPEED

		else:

			motion = deltaPosition / SPEED_REDUCTION


		motion += get_node("../Drone").get_travel()
		defini = true
	move(motion)
	motion = RATIO_SPEED_REDUCTION * motion
	

	if (frames * delta > DELAIS_EXPLOSION):
		queue_free()

func _on_Bomb_exit_tree():
	var explosion = load("res://assets/Explosion.tscn")
	var explosionInstance = explosion.instance()
	explosionInstance.set_pos(get_pos())
	get_parent().add_child(explosionInstance)

	explosionInstance.get_node("Particles2D").set_emitting(true)
	explosionInstance.get_node("Particles2D/Particles2D").set_emitting(true)
	
	
