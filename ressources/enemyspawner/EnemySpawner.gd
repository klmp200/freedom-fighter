extends Position2D

var maxEnemy = 25
var base = 15
var spawnerRange = 2000

var enemy = preload("res://assets/Enemy.tscn")
var civilian = preload("res://assets/Civilian.tscn")


func _ready():

	generate_enemies(maxEnemy, base, spawnerRange)
	generate_civilian(maxEnemy, base, spawnerRange)


func generate_enemies(maxEnemy, base, spawnerRange):
	
	randomize()
	var number = randi()%maxEnemy + base

	for i in range(number):
		
		get_parent().enemyCounter += 1
		var enemyInstance = enemy.instance()
		enemyInstance.set_name("enemy" + str(get_parent().enemyCounter))
		get_parent().call_deferred("add_child", enemyInstance)
		randomize()
		var xrand = get_pos().x + randi()%(2 * spawnerRange) - spawnerRange
		randomize()
		var yrand = get_pos().y + randi()%(2 * spawnerRange) - spawnerRange
		enemyInstance.get_node("EnemyKBody").set_pos(Vector2(xrand, yrand))


# Generate civilians at the beginning of the game
func generate_civilian(maxEnemy, base, spawnerRange):
	
	randomize()
	var number = randi()%maxEnemy + base

	for i in range(number):

		get_parent().civilianCounter += 1
		randomize()
		var xrand = get_pos().x + randi()%(2 * spawnerRange) - spawnerRange
		randomize()
		var yrand = get_pos().y + randi()%(2 * spawnerRange) - spawnerRange
		var civilianInstance = civilian.instance() 
		get_parent().call_deferred("add_child", civilianInstance)
		civilianInstance.get_node("CivilianKBody").set_global_pos(Vector2(xrand, yrand))
		#civilianInstance.get_node("CivilianKBody/sprite").set_global_pos(Vector2(xrand, yrand))
