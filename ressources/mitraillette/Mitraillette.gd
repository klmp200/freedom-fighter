extends KinematicBody2D

#C'est pas le max speed c'est l'option
const DELAIS_SPARITION = 1.00
const MAX_LENGTH_LAUNCH = 750
const RATIO_DIM_REDUCTION = 0.005
const INIT_TAILLE = 1
const SPEED_REDUCTION = 35
const RATIO_SPEED_REDUCTION = 0.99
var frames = 0
var motion = Vector2()
var defini = false


func _ready():
	set_fixed_process(true)

func _init():
	set_meta("Mitraillette",1)


func _fixed_process(delta):
	frames += 1
	set_scale(Vector2(INIT_TAILLE - frames * RATIO_DIM_REDUCTION, INIT_TAILLE - frames * RATIO_DIM_REDUCTION))
	var deltaPosition = get_global_mouse_pos() - get_node("../Drone").get_global_pos()

	if (defini == false):

		motion = deltaPosition / SPEED_REDUCTION
		motion += get_node("../Drone").get_travel()
		set_global_rot(get_global_pos().angle_to_point(get_global_mouse_pos()))

		defini = true
	elif (get_travel() < Vector2(0.01,0.01) && get_travel() > Vector2(-0.01,-0.01)):
		
		self.queue_free()
		
	move(motion)
	motion = RATIO_SPEED_REDUCTION * motion
	
	if (frames * delta > DELAIS_SPARITION):
		queue_free()
	
	# collision between Tanks and Shoots
	if(is_colliding() && get_collider().is_in_group("Enemy")):
		var collidingObject = get_collider().get_parent()
		if (collidingObject.life > 0):
			collidingObject.life -= 1
			self.queue_free()

		else:
			if(collidingObject.get_name().match("?Civilian*")): #create civilians cadaver and add score
				get_parent().kill_civilian(collidingObject)


			elif(collidingObject.get_name().match("enemy*")): #tanks cadaver
				get_parent().kill_tank(collidingObject)


			else:
				get_collider().get_parent().queue_free()
		
