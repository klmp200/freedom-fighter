extends Node2D

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	set_fixed_process(true)

func _fixed_process(delta):
	get_node("Aim").set_pos(get_global_mouse_pos())

func destroy():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	queue_free()