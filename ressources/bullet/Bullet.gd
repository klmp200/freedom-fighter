extends Area2D

var playerPosition
var speed = 300
var lifetime = 5
var timeAlive = 0
var magicalConstant = 50

func _ready():
	
	set_fixed_process(true)
	set_scale(Vector2(0, 0))


func _fixed_process(delta):

	# gestion of lifetime of the bullet
	timeAlive += delta
	set_scale(Vector2(0.05 + timeAlive/magicalConstant, 0.05 + timeAlive/magicalConstant))
	if timeAlive > lifetime:
		self.queue_free()

	#bullet movement
	playerPosition = get_parent().get_node("Drone").get_pos()

	var movementVector = playerPosition - get_pos()
	set_global_pos(get_global_pos() + movementVector.normalized() * delta * speed)
	set_rot(get_pos().angle_to_point(playerPosition))
	
	if overlaps_body(get_node("../Drone")):
		print(get_node("../Drone").HP)
		
		if (get_node("../Drone").HP == 1):
			get_node("../UICanvas/UI").display_ending(get_parent().get_kill(), 0)
		else:
			get_node("../Drone").HP -= 1
		
		queue_free()
	
# THE FUCKING COLLISION WITH DRONE SMALL SHOOTS!!!

func _on_Bullet_body_enter( body ):
	if (body.has_meta("Mitraillette")):
		self.queue_free()
		body.queue_free()


