extends Node

var speed = 100
var position
var randomPlayerDistance
var triggerDistance = 600
var screenHeight = 900
var screenWidth = 1600

var bullet = preload("res://assets/Bullet.tscn")
var bulletCounter = 0
var attackSpeed = 0.3
var bulletTimer = 0
var life = 15

	
func _ready():
	set_fixed_process(true)
	randomize()
	randomPlayerDistance = randi()%200 + 400
	position = get_node("EnemyKBody").get_pos()
	bulletTimer = 0


func _fixed_process(delta):
	

	position = get_node("EnemyKBody").get_global_pos()
	var playerPosition = self.get_parent().get_node("Drone").get_pos()
	var movementVector = playerPosition - position
	var animation = get_node("EnemyKBody/Node/roule")


	# Check if the enemy should move
	if movementVector.length() > randomPlayerDistance and movementVector.length() < triggerDistance:
		get_node("EnemyKBody").set_pos(position + movementVector.normalized() * speed * delta)

		#rotation
		var angle = get_node("EnemyKBody").get_pos().angle_to_point(playerPosition)
		get_node("EnemyKBody").set_rot(angle)


		#animation gestion : if the tank is moving, animation is playing
		if !animation.is_playing():
			get_node("EnemyKBody/Node/roule").play("roule", 1, 1, false)

	elif animation.is_playing():
		animation.stop()


	# Check if the enemy is in range to shoot
	if movementVector.length() <= randomPlayerDistance:

		bulletTimer += delta

		if bulletTimer > 1/attackSpeed:

			bulletCounter += 1
			var bulletInstance = bullet.instance()
			get_parent().add_child(bulletInstance)
			bulletInstance.set_name("bullet" + str(bulletCounter))
			bulletInstance.set_global_pos(position)
			self.bulletTimer = 0

