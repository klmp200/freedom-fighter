extends Node

var enemy = preload("res://assets/Enemy.tscn")
var enemyCounter = 0

var civilian = preload("res://assets/Civilian.tscn")
var civilianCounter = 0

var kill = 0

var cadavre = preload("res://assets/Cadavre.tscn")


var tankNoise = preload("res://ressources/songs/Exploosion.ogg")
var civilianNoise = preload("res://ressources/songs/Cri_de_pucelle.ogg")

var inIntro = true
var introTime = 3

var score = 0
var mapWidth = 1600
var mapHeight = 900

var baseTriggerChance = 50

	
func _ready():

	set_fixed_process(true)


func _fixed_process():

	if civilianCounter == 0:
		get_node("UICanvas/UI").displayEnding(kill, 10)


func kill_tank(tank):

	var cadavreInstance = cadavre.instance()
	add_child(cadavreInstance)
	cadavreInstance.set_global_pos(tank.get_node("EnemyKBody").get_global_pos())
	var cadavreTexture = load("res://ressources/cadavre/cadav_tank.tex")
	cadavreInstance.set_texture(cadavreTexture)
	cadavreInstance.get_node("Noise").set_stream(tankNoise)
	cadavreInstance.playNoise()


	#tank particles
	var particle = load("res://assets/tank_kill.tscn")
	var particleInstance = particle.instance()
	add_child(particleInstance)
	particleInstance.set_global_pos(tank.get_node("EnemyKBody").get_global_pos())
	particleInstance.set_emitting(true)
	particleInstance.get_node("Particles2D").set_emitting(true)

	#add score
	score += 10
	get_node("UICanvas/UI/ScoreLabel").set_text(str(score))

	enemyCounter -= 1
	kill += 1

	tank.queue_free()


func kill_civilian(civilian):

	#launch creepy shit sometimes
	randomize()
	var randAnimation = ( randi()%98 + 1) + score / 200
	if randAnimation > (99 - baseTriggerChance):
		get_node("UICanvas/UI").launch_random_animation()



	var cadavreInstance = cadavre.instance()
	add_child(cadavreInstance)
	cadavreInstance.set_global_pos(civilian.get_node("CivilianKBody").get_global_pos())

	randomize()
	if civilian.randomSprite == 0:
		var randomCadavre = randi()%5 + 1
		var cadavreTexture = load("res://ressources/cadavre/cadav" + str(randomCadavre) + ".tex")
		cadavreInstance.set_texture(cadavreTexture)

	else:
		var randomCadavre = randi()%5 + 3
		var cadavreTexture = load("res://ressources/cadavre/cadav" + str(randomCadavre) + ".tex")
		cadavreInstance.set_texture(cadavreTexture)


	cadavreInstance.get_node("Noise").set_stream(civilianNoise)
	cadavreInstance.playNoise()

	#create particles
	var particle = load("res://assets/human_blood.tscn")
	var particleInstance = particle.instance()
	add_child(particleInstance)
	particleInstance.set_global_pos(civilian.get_node("CivilianKBody").get_global_pos())
	particleInstance.set_emitting(true)
	particleInstance.get_node("Particles2D").set_emitting(true)

	#add score
	score += 15
	get_node("UICanvas/UI/ScoreLabel").set_text(str(score))


	civilianCounter -= 1
	kill += 1


	civilian.queue_free()


func get_kill():

	return kill
